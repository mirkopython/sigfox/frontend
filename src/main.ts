import { createApp } from 'vue'
import App from './App.vue'
import router from '../router'
import axios from 'axios'
import VueAxios from 'vue-axios'

/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'

/* import font awesome icon component */
import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from '@fortawesome/vue-fontawesome'

/* import specific icons */
import { faUserSecret,
         faSimCard,
         faTemperatureThreeQuarters,
         faDroplet,
         faGaugeHigh,
         faTemperatureFull,
         faCalendar,
         faListOl,
         faGlobe,
         faEarthAmericas,
         faSatelliteDish,
         faNetworkWired,
         faSquareFull,
         faComment,
         faPenAlt,
         faCircle
        } from '@fortawesome/free-solid-svg-icons'

/* add icons to the library */
library.add(faUserSecret,
            faSimCard,
            faTemperatureThreeQuarters,
            faDroplet,
            faGaugeHigh,
            faTemperatureFull,
            faCalendar,
            faListOl,
            faGlobe,
            faEarthAmericas,
            faSatelliteDish,
            faNetworkWired,
            faSquareFull,
            faComment,
            faPenAlt,
            faCircle
            )

//axios.defaults.baseURL=import.meta.env.VITE_BASE_URL
//axios.defaults.baseURL="http://sigfox-api/api/message/"

import './assets/base.css'

const app = createApp(App)

app.use(router)
app.use(VueAxios, axios)

/* add font awesome icon component */
app.component('font-awesome-icon', FontAwesomeIcon)
app.component('font-awesome-layers', FontAwesomeLayers)
app.component('font-awesome-layer-text', FontAwesomeLayersText)


app.mount('#app')
