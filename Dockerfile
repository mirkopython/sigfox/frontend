FROM node
COPY . /app

WORKDIR /app

RUN npm install
RUN npm run build
EXPOSE 4173

#CMD [ "npm","run","dev","--", "--host", "5555"  ]
CMD ["npm","run","preview", "--", "--host"]
